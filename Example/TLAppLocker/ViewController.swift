//
//  ViewController.swift
//  TLAppLocker
//
//  Created by ravitilicho on 07/03/2019.
//  Copyright (c) 2019 ravitilicho. All rights reserved.
//

import UIKit
import TLAppLocker

class ViewController: UIViewController {

    var presented: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if !presented {
            let newInstance = TLAppLockerViewController.newInstance(
                withMode: .validate,
                passwordLength: 4,
                themeColor: UIColor.green,
                callback: {successful in
                    if successful {
                        print("All is well")
                    } else {
                        print("Something isn't well")
                    }
            })
            
            if newInstance.vc != nil {
                self.present(newInstance.vc!, animated: true)
            }
            
            presented = true
        }
    }

}

