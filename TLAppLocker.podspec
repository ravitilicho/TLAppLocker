#
# Be sure to run `pod lib lint TLAppLocker.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'TLAppLocker'
  s.version          = '0.1.0'
  s.summary          = 'Simple way to add lock feature to apps'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
This pod enables users to add locking feature into their apps without much hassle. User can configure the length of password as well.
                       DESC

  s.homepage         = 'https://github.com/ravitilicho/TLAppLocker'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'ravitilicho' => 'bitsian.ravi@gmail.com' }
  s.source           = { :git => 'https://github.com/ravitilicho/TLAppLocker.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.swift_versions   = '5.0'
  s.ios.deployment_target = '9.0'

  s.source_files = 'TLAppLocker/Classes/**/*'
  
  # s.resource_bundles = {
  #   'TLAppLocker' => ['TLAppLocker/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
