//
//  AppLocker.swift
//  AppLockDemo
//
//  Created by Ravindar Katkuri on 27/06/19.
//  Copyright © 2019 Tilicho. All rights reserved.
//

import UIKit
//import Lottie

public enum LockerMode {
    case createStep1
    case createStep2
    case change
    case validate
    case delete
}

public class TLAppLockerViewController: UIViewController, CAAnimationDelegate {
    
    @IBOutlet weak var passwordL: UILabel!
    @IBOutlet weak var hintLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var animationInnerView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var animationView: UIView!
    private static var passwordStorageKey = "lockerPassword"
    
    private var callback: (Bool) -> () = {_ in }
    
    var lockerMode: LockerMode = .createStep1 {
        didSet {
            
            if hintLabel != nil {
                setHintLabel()
            }
        }
    }
    
    public var maxPasswordLength: Int = 4
    public var lockerThemeColor: UIColor = .blue
    
    private var password: String = "" {
        didSet {
            printPasswordLabel(withLength: password.count)
        }
    }
    
    private var repeatPassword: String = ""
    private var defaults: Foundation.UserDefaults = Foundation.UserDefaults()
    
    private var DELETE_BUTTON_TAG: Int = 101
    private var CANCEL_BUTTON_TAG: Int = 100
    
//    var animationSuccessView: LOTAnimationView!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        password = ""
        setHintLabel()
        
        animationView.isHidden = true
        self.view.backgroundColor = lockerThemeColor
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setupAnimationView()
    }
    
    private func setupAnimationView() {
        
//        animationSuccessView = LOTAnimationView(name: "success-animation")!
//        animationSuccessView.frame = animationInnerView.frame
        animationInnerView.isUserInteractionEnabled = false
//        animationSuccessView.isUserInteractionEnabled = false

        
//        self.view.addSubview(animationSuccessView)
    }
    
    private func playSuccessAnimation(callback: () -> () = {}) {
        
        containerView.isHidden = true
        animationView.isHidden = false
        
//        animationSuccessView.play { success in
//
            self.dismiss(animated: true, completion: nil)
//        }
    }
    
    private func setHintLabel() {
        
        switch lockerMode {
        case .createStep1:
            hintLabel.text = "Enter your password"
            
        case .createStep2:
            hintLabel.text = "Re-enter your password"
            
        case .validate:
            hintLabel.text = "Enter your password"
            cancelButton.isHidden = true
            
        case .change:
            hintLabel.text = "Enter old password"
            
        case .delete:
            hintLabel.text = "Enter your password"
            
        }
    }
    
    @IBAction func keyboardTapped(_ sender: UIButton) {
        
        print(sender.titleLabel?.text ?? "")
        
        if sender.tag == DELETE_BUTTON_TAG {
            
            password = String(password.dropLast())
            
        } else if sender.tag == CANCEL_BUTTON_TAG {
            
            // Pressing cancel button should invoke the callback with false i.e. unsuccessful
            
            self.dismiss(animated: true, completion: nil)
            self.callback(false)
            
        } else if password.count < maxPasswordLength {
            
            password = password + "\(sender.tag)"
            
        }
        
        if password.count == maxPasswordLength {
            password += "\(sender.tag)"
            performAction()
        }
    }
    
    private func printPasswordLabel(withLength length: Int) {
        
        var labelText = ""
        
        if length <= maxPasswordLength {
            for _ in 0..<length {
                labelText += "*"
            }
            
            for _ in length..<maxPasswordLength {
                labelText += "-"
            }
        }
        
        passwordL.text = labelText
    }
    
    private func performAction() {
        
        switch lockerMode {
        case .createStep1:
            repeatPassword = password
            lockerMode = .createStep2
            password = ""
            
        case .createStep2:
            if repeatPassword == password {
                defaults.set(password, forKey: TLAppLockerViewController.passwordStorageKey)
                defaults.synchronize()
                password = ""
                repeatPassword = ""
                hintLabel.text = "Password set successfully!"
                
                self.callback(true)
                playSuccessAnimation()                                    
                
            } else {
                
                password = ""
                hintLabel.text = "Repeat password doesn't match password."
                passwordL.shake(delegate: self)
            }
            
        case .validate:
            let storedPassword = defaults.string(forKey: TLAppLockerViewController.passwordStorageKey)
            if storedPassword == password {
                self.dismiss(animated: true, completion: {
                    self.callback(true)
                })
            } else {
                password = ""
                hintLabel.text = "Password doesn't match. Please try again."
                passwordL.shake(delegate: self)
            }
        case .change:
            let storedPassword = defaults.string(forKey: TLAppLockerViewController.passwordStorageKey)
            if storedPassword == password {
                password = ""
                lockerMode = .createStep1
            } else {
                password = ""
                hintLabel.text = "Password doesn't match. Please try again."
                passwordL.shake(delegate: self)
            }
        case .delete:
            let storedPassword = defaults.string(forKey: TLAppLockerViewController.passwordStorageKey)
            if storedPassword == password {
                defaults.removeObject(forKey: TLAppLockerViewController.passwordStorageKey)
                password = ""
                self.dismiss(animated: true, completion: {
                    self.callback(true)
                })
            } else {
                password = ""
                hintLabel.text = "Password doesn't match. Please try again."
                passwordL.shake(delegate: self)
            }
            
            
        }
    }
    
    private func clearView() {
        
        password = ""
    }
    
    static public func isPINSet() -> Bool {
        
        let defaults = Foundation.UserDefaults()
        return defaults.string(forKey: passwordStorageKey) != nil || defaults.string(forKey: "lockerPassword") == ""
    }
}

extension TLAppLockerViewController {
    
    public static func newInstance(
        withMode mode: LockerMode,
        passwordLength: Int = 4,
        themeColor: UIColor = UIColor.blue,
        callback: @escaping (Bool) -> () = {_ in}
        ) -> (vc: TLAppLockerViewController?, errorMessage: String?) {
        
        let vc = UIStoryboard(name: "AppLocker", bundle: nil).instantiateViewController(withIdentifier: "TLAppLockerViewController") as! TLAppLockerViewController
        
        if mode == .change || mode == .delete || mode == .validate {
            
            if !isPINSet() {
                return (vc: nil, errorMessage: "Pin is not set yet")
            }
        }
        
        if mode == .createStep1 || mode == .createStep2 {
            
            if isPINSet() {
                return (vc: nil, errorMessage: "Pin is already set")
            }
        }
        
        vc.lockerMode = mode
        vc.maxPasswordLength = passwordLength
        vc.lockerThemeColor = themeColor
        vc.callback = callback
        
        return (vc: vc, errorMessage: nil)
    }
}

extension UIView {
    func shake(delegate: CAAnimationDelegate) {
        let animationKeyPath = "transform.translation.x"
        let shakeAnimation = "shake"
        let duration = 0.6
        let animation = CAKeyframeAnimation(keyPath: animationKeyPath)
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = duration
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0]
        animation.delegate = delegate
        layer.add(animation, forKey: shakeAnimation)
    }
}
